# Tensorflow/keras on Igrida

The aim of this tutorial is to run an Deep Learning example using IGRIDA. This example needs to work on GPU (better but not mandatory) and use Tensorflow and others tools.

We will compare two ways of install/use Tensorflow in IGRIDA context :
* using virtualenv for install all we need
* loading modules already installed and add a virtualenv for complete your installation

## Node reservation

We need to reserve a node in IGRIDA to run our example. 

Then after login on frontal :

    sh $USER@igrida-frontend.irisa.fr

reserve for an hour (`walltime=01:00:00`) a interactive node with GPU (`/gpu_device=1`): 

    oarsub -I -l /gpu_device=1,walltime=1:00:00

If you want to try without GPU, it is also possible, but you need to have enough memory to run installation :

    oarsub -I -l walltime=1:00:00 -p 'mem_node > 5*1024'


## First method: TensorFlow from scratch (or all inclusive with virtualenv!)


We need to install some softwares that can be big enough to block your quota home dir.
So we recommend you (at least at the beginning) to install softwares in `/srv/tempdd/${USER}`. In this cases create a directory `ts_from_scratch` inside 

```bash
MY_INSTALL_DIR=/srv/tempdd/${USER}/ts_from_scratch

mkdir -p $MY_INSTALL_DIR
cd $MY_INSTALL_DIR
```
### Define list of dependencies 

You can create a `requirements.txt` for list all dependencies your python script need:

```bash
echo '
tensorflow
tensorflow_datasets
' > $MY_INSTALL_DIR/requirements.txt

```

Virtualenv is a tool for manage different independent environnements without collision between each.

### Enable a virtualenv module in igrida using spack packages:

    module load spack/py-virtualenv

(this is not mandatory, but recommended to keep coherent version of tools)

### create your virtualenv

    virtualenv -p python3 $MY_INSTALL_DIR/venv

### activate this virtualenv

    source $MY_INSTALL_DIR/venv/bin/activate

### install dependencies inside your virtualenv

Tensorflow as other Deep Learnings frameworks has a big footprint  and installation need lot of disk space in temporary directory. So we need to set the temporary path to use to be sure to have enough space:

    export TMPDIR=/srv/local/$user
    python -m pip install -r $MY_INSTALL_DIR/requirements.txt

(this take a while)

Misc:
- See https://pip.pypa.io/en/stable/user_guide/#requirements-files for good practice about managing the requirements file. For instance lock your dependencies:

  ```bash
  (venv) pip freeze > requirements.lock
  ```

## run a tensorflow/keras example on igrida

This example is extracted from : https://www.tensorflow.org/datasets/keras_example

First we need to write the python code in `mnist_simple_train.py` file:

```python

import tensorflow as tf
import tensorflow_datasets as tfds

# load dataset MNIST

(ds_train, ds_test), ds_info = tfds.load(
    'mnist',
    split=['train', 'test'],
    shuffle_files=True,
    as_supervised=True,
    with_info=True,
)

# normalize images

def normalize_img(image, label):
  """Normalizes images: `uint8` -> `float32`."""
  return tf.cast(image, tf.float32) / 255., label

ds_train = ds_train.map(
    normalize_img, num_parallel_calls=tf.data.experimental.AUTOTUNE)
ds_train = ds_train.cache()
ds_train = ds_train.shuffle(ds_info.splits['train'].num_examples)
ds_train = ds_train.batch(128)
ds_train = ds_train.prefetch(tf.data.experimental.AUTOTUNE)


# build an evaluation pipeline

ds_test = ds_test.map(
    normalize_img, num_parallel_calls=tf.data.experimental.AUTOTUNE)
ds_test = ds_test.batch(128)
ds_test = ds_test.cache()
ds_test = ds_test.prefetch(tf.data.experimental.AUTOTUNE)


# make and train the model

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128,activation='relu'),
  tf.keras.layers.Dense(10)
])
model.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

model.fit(
    ds_train,
    epochs=6,
    validation_data=ds_test,
)
```

### run with GPU:
load modules for CUDA and CuDnn:

    module load spack/cuda spack/cudnn
    python mnist_simple_train.py

### run without GPU
just run 

    python mnist_simple_train.py

## Check footprint of this installation :

    du -hs $MY_INSTALL_DIR/venv

with this kind of output 

    1.2G	/srv/tempdd/andradgu/ts_from_scratch/venv

That means your installation take 1.2 Gb of disk space

## finally deactivate virtualenv

    deactivate

## Second method: use TensorFlow module already installed in IGRIDA and virtualenv for all the others

As first method, we need to define installation directory:

```bash
MY_INSTALL_DIR=/srv/tempdd/${USER}/ts_with_modules

mkdir -p $MY_INSTALL_DIR
cd $MY_INSTALL_DIR
```
copy the same script `mnist_simple_train.py` inside.

    cp ../ts_from_scratch/mnist_simple_train.py .

## Check modules available

    module avail

## Load Tensorflow module already in IGRIDA:

    module load tensorflow/2.4.1-py3.7 

## create and activate the virtualenv 

    module load spack/py-virtualenv
    virtualenv -p python3 $MY_INSTALL_DIR/venv
    source $MY_INSTALL_DIR/venv/bin/activate

## install the others softwares you need :

    export TMPDIR=/srv/local/$user
    python -m pip install tensorflow_datasets

### run with GPU:

load modules for CUDA and CuDnn:

    module load spack/cuda spack/cudnn
    python mnist_simple_train.py


## Check footprint of this installation :

    du -hs $MY_INSTALL_DIR/venv

with this kind of output 

    34M	/srv/tempdd/andradgu/ts_with_modules/venv

That means your installation take only 34 Mb of disk space.


# run inside a passive jobs script 

to run the python code inside a passive job, write this kind of script `run.sh`:

```bash
#!/bin/bash
#OAR -l /gpu_device=1
#OAR -O /srv/tempdd/USER_HERE/igrida.%jobid%.output
#OAR -E /srv/tempdd/USER_HERE/igrida.%jobid%.output

#patch to be aware of "module" inside a job
. /etc/profile.d/modules.sh
set -xv

MY_INSTALL_DIR=/srv/tempdd/${USER}/ts_with_modules

module load spack/py-virtualenv
module load spack/cuda spack/cudnn tensorflow/2.4.1-py3.7

activate () {
  . $MY_INSTALL_DIR/venv/bin/activate
}

activate 

python mnist_simple_train.py
```

- Misc: oarsub don't read bash variables in header `#OAR -O /srv/tempdd/USER_HERE/igrida.%jobid%.output` so you need to set manually your username directory

## Schedule the job in oar stack:

    oarsub -S ./run.sh

# Conclusion

* You can run your custom tools installed with virtualenv in a temporary file system available in IGRIDA.
* This virtualenv isolate your installation from current standard software and allow you to install specific version without pain
* Usage of `module avail` and `module load ...` allow you to reduce the footprint of your installation if the software with correct version are already installed in commons `/srv/soft/igrida/` or `/srv/soft/spack` inside IGRIDA
  * In the case of Tensorflow the reduction of footprint is huge ! 
  * Dont hesitate to query for common installation of useful and popular software for the community to IGRIDA team
